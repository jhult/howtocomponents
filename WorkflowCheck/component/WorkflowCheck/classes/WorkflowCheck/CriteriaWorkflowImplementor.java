/**
 *  Copyright 2011, Bezzotech LLC
 *
 *      http://bezzotech.com
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
 
package WorkflowCheck;

import intradoc.common.*;
import intradoc.data.*;
import intradoc.shared.*;
import intradoc.server.workflow.*;
import java.util.*;

/**
 * One way to execute arbitraty Java Code is by ClassAliasing.  In this
 * case, we want to customize the functionality right before a criteria 
 * workflow is started.  We want to perform specific checks to make sure 
 * a workflow should begin.
 */
public class CriteriaWorkflowImplementor extends WorkflowDocImplementor
{
	/**
	 * Default constructor
	 */
	public CriteriaWorkflowImplementor()
	{
	}

	/**
	 * This code is called by checkCriteriaWorkflow(), which is called in the
	 * services "CHECKIN_NEW" and "CHECKIN_SEL".  This customized code will
	 * make certain that the content has been checked in by a user with the
	 * security role "workflow_contributor." If the user does not have this
	 * role, the check to place this content into a worklow will always fail.
	 */
	public void checkCriteriaWorkflowEx(boolean isUpdate) throws DataException, ServiceException
	{
		// obtain the data about the user performing the checkin
		UserData user = m_service.getUserData();

		// obtain the list of roles for the user, you can also
		// obtain the attribute "account" in this manner.
		Vector roles = user.getAttributes("role");

		// loop through the roles, and look for "workflow_contributor"
		boolean success = false;
		for (int i=0; i<roles.size(); i++)
		{
			UserAttribInfo info = (UserAttribInfo)roles.elementAt(i);
			if (info.m_attribName.equals("workflow_contributor"))
			{
			  SystemUtils.trace("system", "Starting the workflow for the user'" +
					user.m_name +  "' who has the role 'workflow_contributor'.");
				success = true;
				break;
			}
		}

		// if they dont have the correct role, the document should not go
		// into a workflow no matter what.
		if (!success)
		{
			SystemUtils.trace("system", "User does not have the role 'workflow_contributor'. No workflow will be started.");
			return;
		}

		// if they do have the role, perform all the normal checks to determine
		// which workflow, if any, the document should be placed in
		super.checkCriteriaWorkflowEx(isUpdate);
	}
}
