/**
 *  Copyright 2011, Bezzotech LLC
 *
 *      http://bezzotech.com
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package DatabaseProvider;

import java.io.*;
import intradoc.common.*;
import intradoc.data.*;
import intradoc.server.*;
import intradoc.provider.*;


/**
 * When this ServiceHandler is properly merged into the Content Server, this 
 * code can be called by any service type specified in the merged table
 */
public class DatabaseProviderHandler extends ServiceHandler
{
	/**
	 * Default constructor
	 */
	public DatabaseProviderHandler()
	{
	}
	
	/**
	 * Executes a named query against a named database provider, and stores
	 * the results into a named result set.
	 */
	public void executeProviderQuery() throws
		ServiceException, DataException
	{
		// obtain the provider name, the query, and the result set name
		// from the action definition in the service
		String providerName = m_currentAction.getParamAt(0);
		String resultSetName = m_currentAction.getParamAt(1);
		String queryName = m_currentAction.getParamAt(2);
		
		// validate the provider name
		if (providerName == null || providerName.length() == 0)
		{
			throw new ServiceException("You must specify a provider name.");
		}
		
		// validate that the provider is a valid database provider
		Provider p = Providers.getProvider(providerName);
		if (p == null)
		{
			throw new ServiceException("The provider '" + providerName +
				"' does not exist.");
		}
		else if (!p.isProviderOfType("database"))
		{
			throw new ServiceException("The provider '" + providerName + 
				"' is not a valid provider of type 'database'.");
		}
		
		// grab the provider object that does all the work, and scope it to
		// a workspace object for database access, since we can be reasonably
		// certain at this point that the object returned is a Workspace object
		Workspace ws = (Workspace)p.getProvider();
		DataResultSet result = null;
		
		// if they specified a predefined query, execute that
		if (queryName != null && queryName.trim().length() > 0)
		{
			// obtain a JDBC result set with the data in it.  This result set is
			// temporary, and we must copy it before putting it in the binder
			ResultSet temp = ws.createResultSet(queryName, m_binder);
		
			// create a DataResultSet based on the temp result set
			result = new DataResultSet();
			result.copy(temp);
		}
		
		// place the result into the databinder with the appropriate name
		m_binder.addResultSet(resultSetName, result);
		
		// release the JDBC connection assigned to this thread (request)
		// which kills the result set 'temp'
		ws.releaseConnection();
	}
	
	
	/**
	 * This function will execute arbitraty sql against an arbitraty database
	 * provider, and store the results in a named result set.
	 */
	public void executeProviderSql() throws
		ServiceException, DataException
	{
		// obtain the provider name, and the result set name
		// from the action definition in the service
		String providerName = m_currentAction.getParamAt(0);
		String resultSetName = m_currentAction.getParamAt(1);
		
		// check for RawSql
		String rawSql = m_binder.getLocal("RawSql");
		if (rawSql == null || rawSql.length() == 0)
		{
			throw new ServiceException("You must specify a value for 'RawSql'.");
		}
		
		// validate that the provider is a valid database provider
		Provider p = Providers.getProvider(providerName);
		if (p == null || !p.isProviderOfType("database"))
		{
			throw new ServiceException("You the provider '" + providerName + 
				"' is not a valid provider of type 'database'.");
		}
		
		// grab the provider object that does all the work, and scope it to
		// a workspace object for database access, since we can be reasonably
		// certain at this point that the object returned is a Workspace object
		Workspace ws = (Workspace)p.getProvider();

		// obtain a JDBC result set with the data in it.  This result set is
		// temporary, and we must copy it before putting it in the binder
		ResultSet temp = ws.createResultSetSQL(rawSql);
		
		// create a DataResultSet based on the temp result set
		DataResultSet result = new DataResultSet();
		result.copy(temp);
		
		// place the result into the databinder with the appropriate name
		m_binder.addResultSet(resultSetName, result);
		
		// release the JDBC connection assigned to this thread (request)
		// which kills the result set 'temp'
		ws.releaseConnection();
	}
	
	/**
	 * For demo purposes, turn the result set into a raw string that can be
	 * dumped to a web page.
	 */
	public void convertResultSetToString() throws
		ServiceException, DataException
	{
		// obtain the result set name, and the local data value that the
		// string should be placed into
		String resultSetName = m_currentAction.getParamAt(0);
		String stringName = m_currentAction.getParamAt(1);
		
		// get thre result set from the databinder, complain if it isn't present
		ResultSet result = m_binder.getResultSet(resultSetName);
		if (result == null)
		{
			throw new ServiceException("Cannot turn the result set '" + 
				resultSetName + "' into a string. The result set is null.");
		}
		
		// turn the resultSet into a string and place it into the local data
		try
		{
			DataBinder tempBinder = new DataBinder();
			tempBinder.addResultSet(resultSetName, result);
			StringWriter sw = new StringWriter();
			tempBinder.send(sw);
			m_binder.putLocal(stringName, sw.toString());
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
	}
}
