/**
 *  Copyright 2011, Bezzotech LLC
 *
 *      http://bezzotech.com
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package ScheduledEvent;

import intradoc.server.*;
import intradoc.shared.*;
import intradoc.common.*;
import intradoc.data.*;

/**
 * Another way to execute arbitraty Java Code is through filters.  There
 * are many hooks in the server that check to see if the user wishes to execute
 * additional Java code before performing the standard functions.  
 * 
 * Common spots for filters include validation of data before checkin,
 * executing special code upon server startup, and execution of special
 * code at the beginning of a workflow.
 * 
 * This class is for code that needs to be executed on a scheduled basis, but
 * more frequently than a system event. It therefore hooks into the 
 * 'checkScheduledEvents' hook, and will be executed about every five minutes
 */
public class CustomFrequentEvent implements FilterImplementor
{
	/**
	 * Just a quick scheuled event
	 */
	public int doFilter(Workspace ws, DataBinder eventData, ExecutionContext cxt)
		throws DataException, ServiceException
	{
		// 
		trace("executing action...");
		
		// filter executed correctly.  Return FINISHED.
		return FINISHED;
	}
	
	
	/**
	 * Log a trace message to the 'scheduledevents' section
	 */
	protected void trace(String str)
	{
		SystemUtils.trace("scheduledevents", "- custom:frequent - " + str);
	}	
}
