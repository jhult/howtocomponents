/**
 *  Copyright 2011, Bezzotech LLC
 *
 *      http://bezzotech.com
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package AcmeMail;

import java.util.*;

import intradoc.common.*;
import intradoc.data.*;
import intradoc.server.*;
import intradoc.shared.*;

/**
 * This is the code that can be executed by any Idc service that has a 
 * service type of "AcmeMail.AcmeMailService".
 */
public class AcmeMailService extends Service
{
	
	/**
	 * This code will create a html page based on the data in the databinder,
	 * and email it to the list of addresses specified by the user.
	 */
	public void sendMail() throws DataException, ServiceException
	{
		// obtain the list of addresses from the local data that the user specified
		String str = m_binder.getLocal("acmeEmailAddresses");

		// parse the list of comma-seperated addresses, and put it in a vector
		Vector emailAdrs = StringUtils.parseArray(str, ',', '^');
		
		// obtain the complete list of user aliases
		AliasData aliasData = (AliasData)SharedObjects.getTable("Alias");

		// you may also obtain a list of users with this code:
		//    Users users = (Users)SharedObjects.getTable("Users");
		
		// obtain the list of alises specified by the user
		str = m_binder.getLocal("acmeAliases");
		
		// parse the list of comma-seperated aliases, and put it in a vector
		Vector aliases = StringUtils.parseArray(str, ',', '^');
		
		// for each alias in the vector 'aliases', look up which users have this
		// alias, and add them all to the vector 'emailAdrs'
		int num = aliases.size();
		for (int i=0 ; i<num ; ++i)
		{
			String alias = (String)aliases.elementAt(i);
			alias = alias.trim();
			
			// get the table of users that have this alias.  For each
			// user in this list, add them to the email address vector
			String[][] aliasUsers = aliasData.getUsers(alias);
			int numUsers = aliasUsers.length;
			for (int j=0 ; j<numUsers ; j++)
			{
				// get the user data for this user name
				String userName = aliasUsers[j][0];
				UserData data = UserStorage.retrieveUserDatabaseProfileData(userName, m_workspace, this);
				
				// get the email address for this user
				String email = data.getProperty("dEmail");
				
				// add this email address to the vector of addresses, if its not
				// already in there
				if (email != null && email.length() > 0)
				{
					addUniqueStringToVector(emailAdrs, email);
				}
			}
		}
		
		// perform error checking
		if (emailAdrs.size() == 0)
		{
			if (aliases.size() == 0)
			{
				createServiceException(null, "No email addresses, and no " +
						"valid aliases were specified.");
			}
			else
			{
				createServiceException(null, "None of the aliases passes " +
						"were valid.");
			}
		}
		
		// create a comma seperated string from the vector. This is the 
		// opposite function of 'parseArray'
		String emailStr = StringUtils.createString(emailAdrs, ',', '^');
		
		// get the user-specidied subject from the local data
		String subject = m_binder.get("acmeSubject");
		
		// this command is used to send email to the client. The message 
		// sent is a template (ACME_MAIL), and the data to format the
		// message is taken out of m_binder, contained in "this"
		InternetFunctions.sendMailTo(emailStr, "ACME_MAIL", subject, this);
	}
	
	
	/**
	 * Utility function to add a new string to a vector, if it doesn't already
	 * exist in the vector. This is a case insensitive function.
	 */
	protected void addUniqueStringToVector(Vector vect, String str)
	{
		int num = vect.size();
		for (int i=0 ; i<num ; ++i)
		{
			String curStr = (String)vect.elementAt(i);
			if (curStr.equalsIgnoreCase(str))
			{
				return;
			}
		}
		vect.addElement(str);
	}
}
