/**
 *  Copyright 2011, Bezzotech LLC
 *
 *      http://bezzotech.com
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

 package SecurityFilter;

import intradoc.shared.*;
import intradoc.data.*;
import intradoc.common.*;

/**
 * Another way to execute arbitrary Java Code is through filters.  There
 * are many hooks in the server that check to see if the user wishes to execute
 * additional Java code before performing the standard functions.
 *
 * Common spots for filters include validation of data before checkin,
 * executing special code upon server startup, and execution of special
 * code at the beginning of a workflow.
 *
 * Since this one alters the user credentials, it should hook into the
 * "alterUserCredentials" filter. It will get executed with every request,
 * so make sure your code is fast, and safe. 
 */
public class ModifyAttributesFilter implements FilterImplementor
{
	/**
	 * This filter is executed for all requests, and for all users. It is therefore
	 * important to only grant temporary security clearance when you are positive
	 * that it is safe.  In this case, we will grant 'read' access to one account
	 * always, but will only grant 'write' access to a security group for specific
	 * service requests.
	 */
	public int doFilter(Workspace ws, DataBinder binder, ExecutionContext cxt)
		throws DataException, ServiceException
	{
		// grab the 'userData' object from the list of cached objects. This object
		// contains a great deal of info, and is typically read-only. The values can
		// be modified to grant extra security privileges, but it will only last 
		// for one single request
		UserData userData = (UserData)cxt.getCachedObject("TargetUserData");

		// if its a specific service request, modify the security credentials to allow
		// 'write' access to the 'Public' security group for all users. Otherwise, just 
		// grant 'read' access to the 'everybody' account. 
		String idcService = binder.getLocal("IdcService");
		if (idcService == null)
		{
			return CONTINUE;
		}
		
		if (idcService.equals("BLACKHOLE_CHECKIN"))
		{
			// grant the 'contributor' role, if they don't already have it. 
			// This will enable a 'guest' user to check in a file, as long as its 
			// explicitly via the 'BLACKHOLE_CHECKIN' service.
			if (SecurityUtils.isUserOfRole(userData, "contributor") == false)
			{
				grantTemporaryRole(userData, "contributor", 15);
				Report.trace(null, "Granting the 'contributor' role.", null);
			}
		}
		else
		{
			// grant 'read' access to the 'everybody' account, if they don't already have it. 
			// You can place any kind of fancy logic here to allow accounts for specific 
			// users, or users with  specific roles.
			if (SecurityUtils.isAccountAccessible(userData, "everybody", 1) == false)
			{
				grantTemporaryAccount(userData, "everybody", 1);
				Report.trace(null, "Granting access to 'everybody' account.", null);
			}
						
			// grant everybody FULL access to the '#none' account. This is a magic
			// account name that means 'content items without accounts'. This is 
			// useful in the event that you enabled accounts AFTER creating your
			// users. Don't worry about giving FULL access - the users will still
			// be restricted by security group
			if (SecurityUtils.isAccountAccessible(userData, "#none", 15) == false)
			{
				grantTemporaryAccount(userData, "#none", 15);
				Report.trace(null, "Granting access to '#none' account.", null);
			} 
		}
		
		// filter executed correctly.  Return CONTINUE
		return CONTINUE;
	}
	
	/**
	 * This method will modify the security credentials to allow a user to temporarily have
	 * an additional account, with a specified privilege level. This credentials boost is
	 * only good for one single request.
	 * @param userData The UserData object cached in the ExecutionContent, or the Service
	 * @param accountName The name of the security account to grant access to
	 * @param privilege A bit flag for access privilege: 1-read, 2-write, 4-delete, 8-admin
	 * @throws ServiceException
	 */
	protected void grantTemporaryAccount(UserData userData, String accountName, 
		int privilege) throws ServiceException
	{
		// if accounts are not enabled, do nothing
		if (SharedObjects.getEnvValueAsBoolean("UseAccounts", false) == false)
		{
			return;
		}
		
		// Add the account to the user temporarily.
		userData.addAttribute("account", accountName, "" + privilege);
	}
	
	/**
	 * This method will modify the security credentials to allow a user to temporarily have
	 * an additional role, with a specified privilege level. It is only good for one single request.
	 * @param userData The UserData object cached in the ExecutionContent, or the Service 
	 * @param roleName The name of the role to grant.
	 * @param privilege A bit flag for access privilege: 1-read, 2-write, 4-delete, 8-admin
	 * @throws ServiceException
	 */
	protected void grantTemporaryRole(UserData userData, String roleName, 
		int privilege) throws ServiceException
	{
		// Add the role to the user temporarily.
		userData.addAttribute("role", roleName, "" + privilege);
	}
}
