/**
 *  Copyright 2011, Bezzotech LLC
 *
 *      http://bezzotech.com
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package StockQuotes;

import java.util.*;
import java.io.*;
import java.net.*;

import intradoc.common.*;
import intradoc.data.*;
import intradoc.shared.*;
import intradoc.server.*;


/**
 * When this ServiceHandler is properly merged into the server, the code 
 * can be called by any service type specified in the merged table
 */
public class StockQuotesHandler extends ServiceHandler
{
	/**
	 * Default constructor
	 */
	public StockQuotesHandler()
	{
	}
	

	/**
	 * Call this before saving the personalization info to make sure
	 * the data is in the correct format.
	 */
	public void validateStockData() throws ServiceException, DataException
	{
		// if this is a delete, skip the check
		String deletedName = m_binder.getLocal("deletedName");
		if (deletedName != null)
			return;
		
		// look for 'name' and tickerName' in the local data
		String name = m_binder.getLocal("name");
		String tickerName = m_binder.getLocal("tickerName");
				
		// make sure they both exist as positive length strings
		if (name == null || name.trim().length() == 0 || 
			tickerName == null || tickerName.trim().length() == 0)
		{
			throw new ServiceException("Values for 'Company Name' and " +
				"'Ticker Name' must be entered.");
		}
		
		// verify that illegal character do not exist in the strings
		// passed by the user.  Commas in particular mess up parsing.
		if (!isAlphaNumeric(name) || !isAlphaNumeric(tickerName))
		{
			throw new ServiceException("Values for 'Company Name' and " +
				"'Ticker Name' contain illegal characters.");
		}
	}
	

	/**
	 * This is a sample of how to use the stock quotes from a source
	 * such as Yahoo! to personalize the server for a user.  Please note - 
	 * this is a sample meant for low-volume demonstration purposes.  
	 * For a full scale deployment, the Java code should be modified 
	 * to perform batch downloads of the data, as well as server 
	 * side caching of the most popular stocks, as well as Thread 
	 * based error correction in case the network connection is down.
	 * 
	 * Loading the topic info for "stocks" for a user means loading in 
	 * the data from the file:
	 * 
	 *    <server>/data/users/profiles/<ab>/<abcd...>/stocks.hda
	 * 
	 * Where <ab> is the first 2 letters of the username, and <abcd...> is the
	 * full username.
	 */
	public void accessCustomStockQuotes() throws DataException, ServiceException
	{
		// obtain the information about the user making this request
		UserData userData = m_service.getUserData();
		
		// create and initialize a UserProfileManager object for reading
		// in personalization information for this user
		UserProfileManager manager = new UserProfileManager(userData, null, m_service);
		manager.init();
		
		// create a UserProfileEditor object from the manager
		UserProfileEditor editor = manager.getProfileEditor();
		
		// load the TopicInfo for "stocks", which loads up the data in the
		// personalization file "stocks.hda"
		TopicInfo info = editor.loadTopicInfo("stocks");
		
		// obtain the databinder created from the file "stocks.hda"
		DataBinder stockInfoBinder = info.m_data;
		
		// obtain the result set "StockData" from the databinder. It has the
		// columns 'name' and 'tickerName'
		DataResultSet stockNames = (DataResultSet)stockInfoBinder.getResultSet("StockData");
		
		// look for 'name' and tickerName' in the local data
		String name = m_binder.getLocal("name");
		String tickerName = m_binder.getLocal("tickerName");
		
		// make sure there is stock data to look up
		if (stockNames == null)
		{
			if (name == null || tickerName == null)
				return;
			
			// in this case, there is no saved personalization data, but there
			// is local data because the user just requested to add a company 
			// to the list for the first time.
			stockNames = new DataResultSet(new String[] {"name", "tickerName"});
		}
		
		// add the values for 'name' and 'tickerName' from the local data
		// into the result set 'stockNames'
		if (name != null && tickerName != null)
		{	
			Vector row = stockNames.createEmptyRow();
			row.setElementAt(name, 0);
			row.setElementAt(tickerName, 1);
			stockNames.addRow(row);
		}
		
		// get the name of the most recently deleted stock name, in case
		// the action last performed was a delete
		String deletedName = m_binder.getLocal("deletedName");
		
		// create a new result set to hold all the information about 
		// the stock that will be downloaded
		DataResultSet fullStockData = new DataResultSet(new String[] {"name", 
			"tickerName", "price", "date", "time", "change", 
			"openingPrice", "askPrice", "prevClose", "volume" });
		
		// these url strings are Yahoo specific, the beginning points to the
		// cgi machine that sends back raw data, the ending contains parameters
		// about what stock data to send.  The ticker name goes in the middle
		String urlBegin = "http://finance.yahoo.com/d/q?s=";
		String urlEnd   = "&f=sl1d1t1c1oghv";
		
		// for each row in stockNames, go to Yahoo to get the data, parse it,
		// and build up the result set.
		for (stockNames.first(); stockNames.isRowPresent(); stockNames.next())
		{
			// name and tickerName are the first and second rows in stockNames
			name = stockNames.getStringValue(0);
			tickerName = stockNames.getStringValue(1);
			
			// if this name is the deleted stock name, skip it
			if (name.equals(deletedName))
				continue;
			
			// grab the raw data string from Yahoo's financial database.  It will
			// look like this:
			// "tickerName",price,"date","time",change,openingPrice,askPrice,prevClose,volume
			String content = getUrlStringContent(urlBegin + tickerName + urlEnd);
			
			// remove the double-quote characters for a nicer looking display
			content = content.replace('"', ' ');
			
			// parse the comma-seperated string into a vector, using the 
			// backslash character as the escape character
			Vector v = StringUtils.parseArray(content, ',', '\\');
			
			// create an empty row to be filled and placed into the 
			// fullStockData result set
			Vector row = fullStockData.createEmptyRow();
			
			// place the name, and then all other values into the row
			row.setElementAt(name, 0);
			for (int i=1; i<10; i++)
				row.setElementAt(v.elementAt(i-1), i);
			
			// add the row to the result set
			fullStockData.addRow(row);
		}
		
		// get the name that the result set should be called from 
		// the current action
		String resultSetName = m_currentAction.getParamAt(0);
		
		// place the stock data result set into the data binder
		m_binder.addResultSet(resultSetName, fullStockData);
	}
	
	
	/**
	 * This is a utility function that accesses a url and places the data
	 * content of that url into a string.
	 */
	protected String getUrlStringContent(String urlStr) throws ServiceException
	{
		URL url = null;
		InputStream is = null;
		byte bytes[] = new byte[1024];
		StringBuffer strBuf = new StringBuffer();
		try
		{
			url = new URL(urlStr);
			is = url.openStream();
			while (is.read(bytes) > 0)
				strBuf.append(new String(bytes));
		}
		catch (Exception e)
		{
			throw new ServiceException("Unable to read from the url '" +
				urlStr + "'. " + e.getMessage());
		}
		finally
		{
			closeStream(is);
		}
		return strBuf.toString();
	}
	
	
	/**
	 * Safely close an input stream.
	 */
	public void closeStream(InputStream is)
	{
		if (is != null)
		{
			try
			{
				is.close();
			}
			catch (IOException ignore) 
			{
			}
		}
	}
	
	
	/**
	 * A utility function to quickly determine if the string passed
	 * is an alphanumeric string, allowing blank spaces.
	 */
	public boolean isAlphaNumeric(String str)
	{
		int length = str.length();
		char[] chars = new char[length+1];
		str.getChars(0, length, chars, 0);
		for (int i=0; i<length; i++)
		{
			if (!(chars[i] >= 'a' && chars[i] <= 'z') &&
				!(chars[i] >= 'A' && chars[i] <= 'Z') &&
				!(chars[i] >= '0' && chars[i] <= '9') &&
				!(chars[i] == ' '))
						return false;
		}
		return true;
	}
}
