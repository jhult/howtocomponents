/**
 *  Copyright 2011, Bezzotech LLC
 *
 *      http://bezzotech.com
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package DataAccess;

import intradoc.shared.*;
import intradoc.server.*;
import intradoc.data.*;
import intradoc.common.*;
import intradoc.resource.*;
import java.io.*;
import java.util.*;

/**
 * Another way to execute arbitraty Java Code is through filters.  There
 * are many hooks in the server that check to see if the user wishes to execute
 * additional Java code before performing the standard functions.
 *
 * Common spots for filters include validation of data before checkin,
 * executing special code upon server startup, and execution of special
 * code at the beginning of a workflow.
 *
 * Since this one alters the database schema, and should only be performed
 * upon startup, this filter is of type "extraBeforeCacheLoadInit".
 */
public class UpdateDatabaseFilter implements FilterImplementor
{
	/**
	 * The version of this component that is already installed.
	 */
	protected String m_dbVersion = null;

	/**
	 * The current version of this component
	 */
	protected static final String m_version = "1";

	/**
	 * The workspace object that handles all database actions
	 */
	protected Workspace m_workspace = null;

	/**
	 * This filter will prepare the database for the rest of the component.
	 * If the table DataAccessTable does not exist, this filter will create it,
	 * and fill it with initial default values when the content server starts up.
	 * If the table already exists, this filter does nothing.
	 */
	public int doFilter(Workspace ws, DataBinder binder, ExecutionContext cxt)
		throws DataException, ServiceException
	{
		// keep a pointer to the workspace object for other
		// member functions
		m_workspace = ws;

		// check to see if the database has the proper table structure
		// for version 1 of this component.
		if (DBIsValidForComponent())
		{
			return CONTINUE;
		}

		// since the database schema is not valid, it must be updated
		updateDatabase();

		// now update the Config table so that it reflects that this
		// version of the component has been installed
		updateConfigTable();

		// now create a special folder and file in the
		// <server>/data/ directory for this component
		createDataDirectory();

		// filter executed correctly.  Return CONTINUE
		return CONTINUE;
	}

	/**
	 * Checks the "Config" table to see if the installed version of this
	 * component (if any) has the same revision number as the current one.
	 */
	protected boolean DBIsValidForComponent() throws DataException
	{
		// assemble the raw sql
		String sql = "SELECT dValue FROM Config WHERE dName='DataAccessComponent'";

		// execute the sql, and obtain a ResultSet
		ResultSet rset = m_workspace.createResultSetSQL(sql);

		// check for no entries
		if(rset == null || rset.isEmpty())
			return false;

		// set the result set counter to the first (and only)
		// result in the set
		rset.first();

		// obtain the value for the column "dValue"
		m_dbVersion = ResultSetUtils.getValue(rset, "dValue");

		// see if the installed version is newer than this version
		int result = m_dbVersion.compareTo(m_version);
		if (result >= 0)
			return true;

		// default - database needs to be updated
		return false;
	}

	/**
	 * Based on the difference between the current version and the version
	 * listed in the Config table, perform an update of the database.
	 */
	protected void updateDatabase() throws DataException
	{
		if (m_dbVersion == null)
		{
			// create the array of FieldInfo objects and strings
			// needed to create the new table
			FieldInfo cols[] = new FieldInfo[3];
			String tableName = "DataAccess";
			String columnNames[] = {"daID", "daName", "daDescription"};
			String primaryKeys[] = {"daID"};
			for (int i=0; i<3; i++)
			{
				cols[i] = new FieldInfo();
				cols[i].m_name = columnNames[i];
				cols[i].m_isFixedLen = true;
				cols[i].m_maxLen = 30;
			}

			// create the table
			m_workspace.createTable(tableName, cols, primaryKeys);

			// fill it with initial default entries
			String insertSql = "INSERT INTO DataAccess (daID, daName, " +
				"daDescription) values ";
			String values[] = { "('001', 'One', 'The first entry')",
								"('002', 'Two', 'The second entry')",
								"('003', 'Three', 'The third entry')" };

			// execute the sql calls to insert the default data
			for (int j=0; j<values.length; j++)
			{
				m_workspace.executeSQL(insertSql + values[j]);
			}
		}
		else
		{
			// since this is the first and only version of this
			// component, there are no backwards compatibility
			// issues to deal with, so this area is left blank.
		}
	}

	/**
	 * Updates the Config table to reflect the version of this component
	 * that is currently being run.
	 */
	protected void updateConfigTable() throws DataException
	{
		// if this is an upgrade, then the older row that tells the
		// version number must be removed before the new one is added
		if (m_dbVersion != null)
		{
			String deleteSql = "DELETE from Config WHERE dName='DataAccessComponent'";
			m_workspace.executeSQL(deleteSql);
		}

		// insert the proper row into the database table
		String insertSql = "INSERT INTO Config (dSection, dName, dVersion," +
			" dValue) values ('DevUpdate', 'DataAccessComponent', '4.0', '" +
			m_version + "')";
		m_workspace.executeSQL(insertSql);
	}


	/**
	 * This function will create a folder in the data directory so this
	 * component can store important information in its own hda formatted file.
	 */
	protected void createDataDirectory() throws ServiceException
	{
		// obtain the full path to the <server>/data/  directory
		String dataDir = DirectoryLocator.getAppDataDirectory();

		// create the directory <server>/data/dataaccess/
		FileUtils.checkOrCreateDirectory(dataDir + "/dataaccess", 5);

		// create the file <server>/data/dataaccess/data.hda  if needed
		File dataFile = new File(dataDir + "/dataaccess/data.hda");
		if (!dataFile.exists())
		{
			// fill the data file with default databinder data
			DataBinder binder = new DataBinder();
			binder.putLocal("fileIsNew", "true");

			// create a sample result set that just has NAME1,DATA1,
			// NAME2,DATA2, etc. for rows
			DataResultSet drset = new DataResultSet(new String[]{"name", "data"});
			for (int i=0; i<4; i++)
			{
				Vector row = drset.createEmptyRow();
				row.setElementAt("NAME" + String.valueOf(i), 0);
				row.setElementAt("DATA" + String.valueOf(i), 1);
				drset.addRow(row);
			}

			// place this result set into the databinder
			binder.addResultSet("GenericData", drset);

			// save the databinder to the data file
			ResourceUtils.serializeDataBinder(dataDir + "/dataaccess",
					"data.hda", binder, true /*write*/, false /*file need not exist*/);
		}
	}
}
