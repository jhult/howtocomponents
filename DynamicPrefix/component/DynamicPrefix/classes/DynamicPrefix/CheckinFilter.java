/**
 *  Copyright 2011, Bezzotech LLC
 *
 *      http://bezzotech.com
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package DynamicPrefix;

import intradoc.shared.*;
import intradoc.common.*;
import intradoc.data.*;

/**
 * Another way to execute arbitraty Java Code is through filters.  There
 * are many hooks in the serevr that check to see if the user wishes to execute
 * additional Java code before performing the standard functions.
 *
 * Common spots for filters include validation of data before checkin,
 * executing special code upon server startup, and execution of special
 * code at the beginning of a workflow.
 *
 * Since this one causes a different dDocName to be automatically generated
 * depending on the document type upon checkin, the ideal place for this
 * filter to be executed is in 'validateStandard'
 */
public class CheckinFilter implements FilterImplementor
{

	/**
	 * This filter reads in the dDocType for this document, then looks to see if
	 * a special AutoNumberPrefix exists for this dDocType.  If so, it will
	 * override the default dDocType with the preferred one.
	 */
	public int doFilter(Workspace ws, DataBinder binder, ExecutionContext cxt)
		throws DataException, ServiceException
	{
	  // obtain the dDocType
		String type = binder.getLocal("dDocType");

		// look up the preconfigured prefix for this data type from the
		// environment (either config.cfg or dynamic_prefix_environment.cfg)
		String prefix = binder.getEnvironmentValue(type + "_prefix");

		// if the prefix value exists, place it in the local data with the name
		// 'AutoNumberPrefix'.  This will temporarilly override the value in
		// the config.cfg file for this checkin ONLY.
		if (prefix != null)
		{
      binder.putLocal("AutoNumberPrefix", prefix);
		}

		// filter executed correctly.  Return CONTINUE
		return CONTINUE;
	}
}
