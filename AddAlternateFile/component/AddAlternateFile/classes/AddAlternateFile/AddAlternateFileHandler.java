/**
 *  Copyright 2011, Bezzotech LLC
 *
 *      http://bezzotech.com
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package AddAlternateFile;

import java.util.*;
import java.io.*;
import java.net.*;

import intradoc.common.*;
import intradoc.data.*;
import intradoc.shared.*;
import intradoc.server.*;


/**
 * When this ServiceHandler is properly merged into the server, the code 
 * can be called by any service type specified in the merged table
 */
public class AddAlternateFileHandler extends ServiceHandler
{
	/**
	 * Default constructor
	 */
	public AddAlternateFileHandler()
	{
	}
	
	/**
	 * This code is meant to be executed any time the standard action 'addFiles'
	 * is called. It will perform some extra steps to determine if it should create
	 * an XML file as the alternate file, and then call the standard 'addFiles' code.
	 */
	public void addFiles() throws ServiceException, DataException
	{
		// pull the two custom flags from this check in request
		String altFile = m_binder.getLocal("alternateFile");
		String makeAltFileStr = m_binder.getLocal("createAlternateFile");
		
		// if no alternate file exists, and the flag 'createAlternateFile' was sent
		// along in the request, generate an alternate file		
		boolean makeAltFile = StringUtils.convertToBool(makeAltFileStr, false);
		if ((altFile == null || altFile.length() == 0) && makeAltFile)
			createAlternateTextFile();
		
		// call the next method called 'addFiles' in the load order. Typically this
		// will just be the one that shipped with the content server.
		m_service.doCodeEx("addFiles", this);
	}

	/**
	 * Creates an alternate file for this content item. The alternate file will be the
	 * one stored in the weblayout directory, and indexed by the search engine. Primary
	 * files can be generated in a similar fashion.
	 */
	protected void createAlternateTextFile() throws ServiceException
	{
		String tempDir = DataBinder.getTemporaryDirectory();
		String name, fullName, fileString, format, extension;
		Writer writer = null;
		try
		{
			// determine what the format of the alternate file will be. It
			// can be 'text/html', or 'text/xml', or 'text/plain'
			format = m_binder.getAllowMissing("AddAlternateFileFormat");
			if (format == null || format == "")
				format = "text/hcsp";
			
			// likewise, the extension must also be fixed
			extension = m_binder.getAllowMissing("AddAlternateFileExtension");
			if (extension == null || extension == "")
				extension = ".hcsp";
			
			// set the format, this will determine how it gets processed by
			// the indexer and the refinery
			m_binder.putLocal("alternateFile:format", format);

			// determine a name for the alternate file, not terribly important
			String docName = m_binder.getLocal("dDocName");
			name = docName.toLowerCase() + "_alt" + extension;
			m_binder.putLocal("alternateFile", name);
			
			// determine a temp location to build the alternate file
			fullName = tempDir + DataBinder.getNextFileCounter() + extension;

			// the path to the temp file's location
			m_binder.putLocal("alternateFile:path", fullName);
			
			// add the temp file to the binder
			m_binder.addTempFile(fullName);
			
			// see if the text for the alternate file was send along in the request
			fileString = m_binder.getLocal("alternateFileText");
			
			// if it was not sent, generate a text file based on an IdocScript
			// template, and 
			if (fileString == null || fileString.trim().length() == 0)
			{
				// determine what the name of the IdocScript template is that will
				// be used to create the temp file
				String template = m_binder.getAllowMissing("AddAlternateFileTemplate");
				if (template == null || template.length() == 0)
					template = "ADD_ALT_FILE_TEMPLATE";

				// render the IdocScript template into a string based on the data currently
				// in the service object, which includes the binder and user context
				fileString = m_service.createMergedPage(template);
			}
			
			// open the file object
			writer = FileUtils.openDataWriter(
				new File(fullName), FileUtils.m_javaSystemEncoding);

			// write the string to it
			writer.write(fileString);
		}
		catch (IOException e)
		{
			throw new ServiceException("Unable to generate the alternate file.", e);
		}
		finally
		{
			// close the write object
			FileUtils.closeObject(writer);
		}	
	}
}
